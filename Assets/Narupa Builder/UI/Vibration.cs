﻿// Copyright (c) Intangible Realities Laboratory. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

/// <summary>
/// Describes a vibration in terms of duration, frequency and amplitude.
/// </summary>
[CreateAssetMenu(fileName = "Vibration", menuName = "Vibration")]
public class Vibration : ScriptableObject
{
    [SerializeField]
    private float duration;

    [SerializeField]
    private float frequency;

    [SerializeField]
    private float amplitude;

    /// <summary>
    /// The duration of the vibration in seconds.
    /// </summary>
    public float Duration => duration;

    /// <summary>
    /// The frequency of the vibration in hertz.
    /// </summary>
    public float Frequency => frequency;

    /// <summary>
    /// The amplitude of the vibration.
    /// </summary>
    public float Amplitude => amplitude;
}