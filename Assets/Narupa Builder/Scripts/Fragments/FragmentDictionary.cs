// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Provides fragments from folders on the user's computer.
    /// </summary>
    public static class FragmentDictionary
    {
        /// <summary>
        /// Load a set of fragments from the StreamingAssets folder with the given name,
        /// returning references to these files.
        /// </summary>
        /// <remarks>
        /// The individual fragments are not imported until
        /// <see cref="IFragmentProvider.GetFragmentAsync" /> is called.
        /// </remarks>
        public static IReadOnlyList<IFragmentProvider> GetStreamingAssetsFragments(string folder)
        {
            var filepath = $"{Application.streamingAssetsPath}/{folder}/";
            var files = Directory.GetFiles(filepath, "*.mol2")
                                 .Concat(Directory.GetFiles(filepath, "*.pdb"))
                                 .Select(GetFragmentFromFile)
                                 .ToList();
            return files;
        }

        /// <summary>
        /// Get a <see cref="IFragmentProvider" /> which references the given filename.
        /// </summary>
        private static IFragmentProvider GetFragmentFromFile(string filename)
        {
            return new FragmentFileReference(filename);
        }
    }
}