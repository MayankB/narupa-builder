﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NarupaBuilder;
using NarupaBuilder.Tools;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using Valve.VR;

public class ToolScaler : MonoBehaviour
{
    [SerializeField]
    private ToolRunner toolRunner;
    
    [SerializeField]
    private float maxValue = 1f;

    [SerializeField]
    private float minValue = 0.1f;

    [SerializeField]
    private float speed = 0.1f;

    [Header("Actions")]
    [SerializeField]
    private SteamVR_Action_Boolean increaseToolSize;

    [SerializeField]
    private SteamVR_Action_Boolean decreaseToolSize;

    [SerializeField]
    private SteamVR_Input_Sources inputSource;

    [SerializeField]
    private UnityEvent onToolSizeChanged;

    private void Awake()
    {
        Assert.IsNotNull(toolRunner);
        Assert.IsNotNull(increaseToolSize);
        Assert.IsNotNull(decreaseToolSize);
    }

    private void Update()
    {
        if (increaseToolSize.GetState(inputSource))
        {
            ChangeToolSize(speed * Time.deltaTime);
        }
        else if (decreaseToolSize.GetState(inputSource))
        {
            ChangeToolSize(-speed * Time.deltaTime);
        }
    }

    private void ChangeToolSize(float deltaSize)
    {
        var newValue = Mathf.Clamp(toolRunner.RelativeToolSize + deltaSize,
                                   minValue,
                                   maxValue);
        if (toolRunner.RelativeToolSize != newValue)
        {
            toolRunner.RelativeToolSize = newValue;
            onToolSizeChanged?.Invoke();
        }
    }
}