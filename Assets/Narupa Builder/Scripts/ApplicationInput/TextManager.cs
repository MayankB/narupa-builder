﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace NarupaBuilder
{
    public class TextManager : MonoBehaviour
    {
        public TextMeshPro activeToolText;
        public TextMeshPro leftPanelText;
        public TextMeshPro rightPanelText;
        public TextMeshPro upPanelText;
        public TextMeshPro downPanelText;

    }
}