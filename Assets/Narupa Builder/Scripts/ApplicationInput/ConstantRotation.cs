﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

public class ConstantRotation : MonoBehaviour
{
    [SerializeField]
    private float angularSpeed;

    private void Update()
    {
        transform.localRotation = Quaternion.AngleAxis(angularSpeed * Time.deltaTime, Vector3.up) *
                                  transform.localRotation;
    }
}