// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using TMPro;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Displays a message if <see cref="ActionsManager"/> has an error.
    /// </summary>
    public class ErrorMessageUi : MonoBehaviour
    {
        [SerializeField]
        private ActionsManager actionsManager;


        [SerializeField]
        private TextMeshProUGUI errorText;

        [SerializeField]
        private RectTransform errorUi;


        private void Update()
        {
            if (!string.IsNullOrEmpty(actionsManager.ActionErrorText))
            {
                errorUi.gameObject.SetActive(true);
                errorText.SetText(actionsManager.ActionErrorText);
            }
            else
            {
                errorUi.gameObject.SetActive(false);
            }
        }
    }
}