// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Narupa_Builder.Scripts.ApplicationInput;
using UnityEngine;
using UnityEngine.Events;

namespace NarupaBuilder.Tools
{
    [CreateAssetMenu(fileName = "Menu Item", menuName = "Menu Item")]
    public class MenuItem : ScriptableObject, IMenuItem
    {
        [SerializeField]
        private string displayName;

        [SerializeField]
        private Sprite icon;

        public string DisplayName => displayName;
        public Sprite Icon => icon;

        public void PerformAction()
        {
            performAction?.Invoke();
        }

        [SerializeField]
        private PerformActionEvent performAction;

        [Serializable]
        public class PerformActionEvent : UnityEvent
        {
        }
    }
}