// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    public class GuidelinesVisualiser : BuilderVisualisation
    {
        private static void RectifyMultipleGuidelinesForSameCore(List<GuidelineData> data)
        {
            var toRemove = new List<GuidelineData>();
            var toAdd = new List<GuidelineData>();
            foreach (GuidelineData gDA in data)
            {
                foreach (GuidelineData gDB in data)
                {
                    if (gDA.core == gDB.core)
                    {
                        var synthesisPositions = new List<Vector3>();
                        for (int i = 0; i < gDA.positions.Count; i++)
                        {
                            synthesisPositions.Add((gDA.positions[i] + gDB.positions[i]) / 2);
                        }

                        toAdd.Add(new GuidelineData(gDA.core, synthesisPositions));
                        toRemove.Add(gDA);
                        toRemove.Add(gDB);
                    }
                }
            }

            foreach (GuidelineData removeThis in toRemove)
            {
                data.Remove(removeThis);
            }

            data.AddRange(toAdd);
        }

        public void SetGuidelines(List<GuidelineData> data)
        {
            RectifyMultipleGuidelinesForSameCore(data);

            if (data.Count == 0)
            {
                ClearFrame();
            }
            else
            {
                var particles = new List<Particle>();
                var bonds = new List<Bond>();

                foreach (GuidelineData gD in data)
                {
                    var core = new Atom(gD.core.Element, 0, gD.core.Pose);
                    var guides = gD
                                 .positions
                                 .Select(v => new GuidelineParticle(
                                             0,
                                             v + gD.core.Pose.Position))
                                 .ToList();
                    var guideBonds = guides.Select(p => new Bond(0, p, core, 1)).ToList();
                    bonds.AddRange(guideBonds);
                    particles.AddRange(guides);
                    particles.Add(core);
                }

                SetupRenderer(particles, bonds);
            }
        }
    }
}