﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Narupa.Core.Math;
using Narupa.Core.Science;
using NarupaBuilder.OpenBabel;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace NarupaBuilder
{
    public static class ImportExport
    {
        public delegate void
            OnProcessFinishedDelegate(
                bool executed); //these guys are for locking things when minimizing

        public static event OnProcessFinishedDelegate OnProcessFinished;

        #region Export Methods

        public static bool Export(BondsAndParticles singleExportFrame, string exportPath)
        {
            if (exportPath == "" || singleExportFrame.Particles.Count == 0)
            {
                return false;
            }

            File.WriteAllText(exportPath, "");

            string extension = Path.GetExtension(exportPath);
            switch (extension)
            {
                case ".xyz":
                    //ExportXYZ(exportFrameData, exportPath);
                    break;
                case ".pdb":
                    ExportPDB(singleExportFrame, exportPath);
                    break;
                case ".mol2":
                    ExportMol2(singleExportFrame, exportPath);
                    break;
                default:
                    ExportPDB(singleExportFrame, exportPath);
                    break;
            }

            return true;
        }

        #region Export PDB Methods

        private static void ExportPDB(BondsAndParticles singleExportFrame, string exportPath)
        {
            StreamWriter writer = new StreamWriter(exportPath, true);

            //Add the header section of the pdb
            PDBHeader(writer);

            int index = 1;
            int nameIndex = 0;
            string residueName = DefaultPDBGroup;
            int residueSeq = DefaultPDBGroupIndex;
            List<BondsAndParticles> residues = singleExportFrame.BreakIntoResiduesAndOrder();
            Debug.Log("Number of residues: " + residues.Count);

            for (int rIndex = 0; rIndex < residues.Count; rIndex++)
            {
                var prevElement = Element.Virtual;
                foreach (var atom in residues[rIndex].Atoms)
                {
                    //atoms must have unique names from the previous one, so this helps distinguish them
                    if (atom.Element != prevElement)
                    {
                        nameIndex = 1;
                    }
                    else
                    {
                        nameIndex++;
                    }

                    prevElement = atom.Element;

                    PDBParticleLine(writer, atom, PDBHetaAtom,
                                    nameIndex, index, residueName + residueSeq, residueSeq);
                    atom.CurrentIndex =
                        (uint) (index); ///THIS STEP COULD CAUSE ERRORS?
                    index++;
                }

                nameIndex = 0;
                residueSeq++; //better way to do this?
            }

            residueSeq--;
            // Add the terminal line for particles
            PDBTerminal(writer, index, residueName + residueSeq, residueSeq);

            //Now write the bonds
            PDBWriteBonds(writer, residues);

            writer.WriteLine(PDBEnd);

            writer.Close();
        }

        private static void PDBParticleLine(TextWriter writer,
                                            Atom particle,
                                            string atomType,
                                            int nameIndex,
                                            int index,
                                            string residueName,
                                            int residueSeq,
                                            float occupancy = 1.00f,
                                            float temp = 0.00f)
        {
            string field = " ";
            string line = " ";
            line = line.PadRight(PDBLength - 1);

            //Atom type
            field = atomType.PadRight(6);
            line = line.Insert(0, field);
            //Atom index
            field = index.ToString().PadLeft(5);
            line = line.Insert(6, field);

            //Atom name

            field = particle.Element.GetSymbol() +
                    nameIndex.ToString();
            field = field.PadLeft(4);
            line = line.Insert(12, field);

            //Residue name
            field = residueName.PadLeft(3);
            line = line.Insert(17, field);

            //Residue sequence
            field = residueSeq.ToString().PadLeft(4);
            line = line.Insert(22, field);

            //Atom coordinates
            Vector3 position = particle.Pose.Position * AngstromScale;
            field = String.Format("{0:0.000}", position.x).PadLeft(8);
            line = line.Insert(30, field);
            field = String.Format("{0:0.000}", position.y).PadLeft(8);
            line = line.Insert(38, field);
            field = String.Format("{0:0.000}", position.z).PadLeft(8);
            line = line.Insert(46, field);

            //Occupancy
            field = String.Format("{0:0.00}", occupancy).PadLeft(6);
            line = line.Insert(54, field);

            //Temperature
            field = String.Format("{0:0.00}", temp).PadLeft(6);
            line = line.Insert(60, field);

            //Atom symbol
            field = particle.Element.GetSymbol().PadLeft(2);
            line = line.Insert(76, field);
            line = line.Remove(PDBLength);
            writer.WriteLine(line);
        }

        private static void PDBTerminal(TextWriter writer,
                                        int index,
                                        string residueName,
                                        int residueSeq)
        {
            string field = " ";
            string line = " ";
            line = line.PadRight(79);

            field = PDBTer.PadRight(6);
            line = line.Insert(0, field);
            field = index.ToString().PadLeft(5);
            line = line.Insert(6, field);
            field = residueName.PadLeft(3);
            line = line.Insert(17, field);
            field = residueSeq.ToString().PadLeft(4);
            line = line.Insert(22, field);

            line = line.Remove(PDBLength);

            writer.WriteLine(line);
        }

        private static void PDBWriteBonds(TextWriter writer,
                                          IReadOnlyList<BondsAndParticles> residues)
        {
            string line = "";
            string field = "";
            int index = 0;
            for (int rIndex = 0; rIndex < residues.Count; rIndex++)
            {
                for (int i = 0; i < residues[rIndex].Particles.Count; i++)
                {
                    List<int> indices = residues[rIndex].Particles[i].BondedParticles
                                                        .Select(particle => (int) particle
                                                                    .CurrentIndex)
                                                        .ToList();
                    //     var indices = new List<int>();
                    // foreach (Particle p in residues[rIndex].Particles[i].BondedParticles){
                    //     indices.Add(p.CurrentIndex);
                    // }
                    Debug.Log("particle : " + residues[rIndex].Particles[i].CurrentIndex);
                    foreach (int id in indices)
                    {
                        Debug.Log(" is bonded to: " + id);
                    }

                    indices.Sort();
                    indices.Reverse();
                    while (indices.Count > 0)
                    {
                        line = " ";
                        line = line.PadRight(PDBLength - 1);

                        field = PDBConnect.PadRight(6);
                        line = line.Insert(0, field);

                        field = (index + 1).ToString().PadLeft(5);
                        line = line.Insert(6, field);

                        int connectCounter = 0;
                        int currentStartIndex = 11;

                        while (connectCounter < 4)
                        {
                            field = (indices[indices.Count - 1]).ToString().PadLeft(5);
                            line = line.Insert(currentStartIndex, field);
                            indices.RemoveAt(indices.Count - 1);

                            if (indices.Count == 0)
                                break;
                            connectCounter++;
                            currentStartIndex += 5;
                        }

                        line = line.Remove(PDBLength);

                        writer.WriteLine(line);
                    }

                    index++;
                }
            }
        }
        //Add the particles
        // var allParticles = new List<Particle>(exportFrameData.Particles);
        // var chainParticles = new List<Particle>();
        // var sortedAtoms = new List<Particle>();


        // while (allParticles.Count > 0)
        // {
        //     TransformModule.CreateChainList(chainAtoms, allAtoms[0], allAtoms[0]);
        //     OrderAtomsByValence(chainAtoms);
        //     for (int i = 0; i < chainAtoms.Count; i++)
        //     {
        //         if (i != 0 && chainAtoms[i].Symbol != chainAtoms[i - 1].Symbol)
        //             atomNameIndex = 1;
        //         else
        //             atomNameIndex++;
        //         Atom atom = chainAtoms[i];
        //         PDBAtomLine(writer, atom, PDBHetaAtom, atomNameIndex, index, residueName + residueSeq, residueSeq);
        //         allAtoms.Remove(atom);
        //         index++;
        //     }
        //     sortedAtoms.AddRange(chainAtoms);
        //     chainAtoms = new List<Atom>();
        //     atomNameIndex = 0;
        //     residueSeq++;
        // }

        // residueSeq--;
        // //Add the terminal line
        // PDBTerminal(writer, index, residueName + residueSeq, residueSeq);

        // //Add the connections
        // PDBConnections(writer, sortedAtoms);

        // writer.WriteLine(PDBEnd);

        // writer.Close();
        private static void PDBHeader(TextWriter writer)
        {
            string field = " ";
            string line = " ";
            line = line.PadRight(PDBLength - 1);
            field = PDBHeaderStart.PadRight(6);
            line = line.Insert(0, field);
            field = PDBHeaderDescription;
            line = line.Insert(10, field);
            field = DateTime.Today.Date.ToString("dd/MM/yyyy");
            line = line.Insert(50, field);
            field = PDBHeaderId;
            line = line.Insert(62, field);
            line = line.Remove(PDBLength);

            writer.WriteLine(line);
        }

        #endregion

        #endregion

        #region Export Mol2 Methods

        private static void ExportMol2(BondsAndParticles singleExportFrame, string exportPath)
        {
            StreamWriter writer = new StreamWriter(exportPath, true);
            Mol2Header(writer); //header not needed for file, but for convenience

            var residues = singleExportFrame.BreakIntoResiduesAndOrder();

            Mol2WriteMoleculeType(writer, residues);

            Mol2WriteParticles(writer, residues);

            Mol2WriteBonds(writer, residues);

            writer.Close();
        }


        private static void Mol2WriteMoleculeType(StreamWriter writer,
                                                  IReadOnlyList<BondsAndParticles> residues)
        {
            string line = mol2MoleculeType;
            writer.WriteLine(line);
            line = "narupa builder molecule";
            writer.WriteLine(line);
            int particleCount = 0;
            int bondCount = 0;
            for (int i = 0; i < residues.Count; i++)
            {
                particleCount += residues[i].Particles.Count;
                bondCount += residues[i].Bonds.Count;
            }

            line = particleCount.ToString() + " " + bondCount.ToString() + " 0 0 0";
            writer.WriteLine(line);
            line = "SMALL";
            writer.WriteLine(line);
            //line = "NO_CHARGES";
            line = "GASTEIGER";
            writer.WriteLine(line);
            Mol2SpaceLine(writer);
        }

        private static void Mol2WriteParticles(StreamWriter writer,
                                               IReadOnlyList<BondsAndParticles> residues)
        {
            Mol2ParticleLineHeader(writer);
            int index = 1;
            int nameIndex = 0;
            for (int rIndex = 0; rIndex < residues.Count; rIndex++)
            {
                var prevElement = Element.Virtual;
                foreach (var atom in residues[rIndex].Atoms)
                {
                    if (atom.Element != prevElement)
                    {
                        nameIndex = 1;
                    }
                    else
                    {
                        nameIndex++;
                    }

                    prevElement = atom.Element;

                    Mol2ParticleLine(writer, atom, index, nameIndex,
                                     rIndex + 1);
                    atom.CurrentIndex = (uint) (index);
                    index++;
                }

                nameIndex = 0;
            }

            Mol2SpaceLine(writer);
        }

        private static void Mol2WriteBonds(StreamWriter writer,
                                           IReadOnlyList<BondsAndParticles> residues)
        {
            Mol2BondLineHeader(writer);
            int index = 1;
            for (int rIndex = 0; rIndex < residues.Count; rIndex++)
            {
                for (int i = 0; i < residues[rIndex].Bonds.Count; i++)
                {
                    string line = index.ToString() + " ";
                    line += residues[rIndex]
                            .Bonds[i].A.CurrentIndex.ToString() + " ";
                    line += residues[rIndex]
                            .Bonds[i].B.CurrentIndex.ToString() + " ";
                    line += residues[rIndex].Bonds[i].BondOrder;
                    writer.WriteLine(line);
                    index++;
                }
            }
        }

        private static void Mol2Header(StreamWriter writer)
        {
            string line = "#exported by Narupa Builder at " +
                          DateTime.Today.Date.ToString("dd/MM/yyyy");
            writer.WriteLine(line);
            Mol2SpaceLine(writer);
        }

        private static void Mol2SpaceLine(TextWriter writer)
        {
            string line = " ";
            writer.WriteLine(line);
        }

        private static void Mol2ParticleLineHeader(TextWriter writer)
        {
            string line = mol2Atom;
            writer.WriteLine(line);
        }

        private static void Mol2ParticleLine(TextWriter writer,
                                             Atom particle,
                                             int index,
                                             int nameIndex,
                                             int residueIndex)
        {
            string line = "";
            //index
            line += index.ToString() + " ";

            //atomname+index
            line += particle.Element.GetSymbol() +
                    nameIndex.ToString() + " ";

            //position
            Vector3 position = particle.Pose.Position;
            line += String.Format("{0:0.000}", position.x * 10f) + " ";
            line += String.Format("{0:0.000}", position.y * 10f) + " ";
            line += String.Format("{0:0.000}", position.z * 10f) + " ";

            //atomtype (hybridization)
            if (particle.Element != Element.Hydrogen)
            {
                int spVal = particle.CommonStericNumber -
                            (particle.TotalBondOrder - particle.BondedParticles.Count) - 1;
                line += particle.Element.GetSymbol() + "." +
                        spVal.ToString() + " ";
            }
            else
            {
                line += "H ";
            }

            //residue information
            line += residueIndex.ToString() + " N" + residueIndex.ToString();
            writer.WriteLine(line);
        }

        private static void Mol2BondLineHeader(StreamWriter writer)
        {
            string line = mol2Bond;
            writer.WriteLine(line);
        }

        #endregion

        #region Import Methods

        /// <summary>
        /// Imports a given molecular structure file.
        /// </summary>
        /// <param name="path">Path to the molecular structure file.</param>
        /// <returns> A <see cref="BondsAndParticles"/> representing the structure. </returns>
        /// <exception cref="InvalidOperationException">Thrown when the file type is not supported.</exception>
        public static BondsAndParticles Import(string path)
        {
            if (OpenBabelSetup.IsOpenBabelAvailable)
            {
                var molecule = OpenBabelImport.Import(path);
                return molecule;
            }
            switch (Path.GetExtension(path))
            {
                case ".pdb":
                    return ImportPDB(path);
                case ".mol2":
                    return ImportMol2(path);
                default:
                    throw new InvalidOperationException($"Cannot import file {path}");
            }
        }

        private static BondsAndParticles ImportMol2(string path)
        {
            var frameData = new BondsAndParticles();
            ReadImportFileMol2(path, frameData);
            return frameData;
        }
        
        private static void ReadImportFileMol2(string path, BondsAndParticles data)
        {
            StreamReader reader = new StreamReader(path);

            string line = reader.ReadLine();
            while (!line.StartsWith("@"))
            {
                line = reader.ReadLine();
            }

            if (line.StartsWith(mol2MoleculeType))
            {
                line = reader.ReadLine();

                string[] moleculeInfo = new string[4];
                for (int i = 0; i < moleculeInfo.Length; i++)
                {
                    moleculeInfo[i] = line;
                    line = reader.ReadLine();
                }

                var moleculeValues = new List<int>();

                var newString = moleculeInfo[1];
                bool hasntReachedEnd = true;
                newString = TrimWhiteSpace(newString);
                while (hasntReachedEnd)
                {
                    int spacePos = newString.IndexOf(" ");
                    if (spacePos < 0)
                    {
                        spacePos = newString.Length;
                        hasntReachedEnd = false;
                    }

                    moleculeValues.Add(int.Parse(newString.Substring(0, spacePos), CultureInfo.InvariantCulture));
                    if (hasntReachedEnd)
                    {
                        newString =
                            (newString.Substring(spacePos + 1, newString.Length - (spacePos + 1)));
                    }
                }

                //maybe none of this is needed
            }

            while (!line.StartsWith("@"))
            {
                line = reader.ReadLine();
            }

            if (line.StartsWith(mol2Atom))
            {
                line = reader.ReadLine();
                line = TrimWhiteSpace(line);

                while (!line.StartsWith("@") && line.Length != 0)
                {
                    int spacePos = line.IndexOf(" ");
                    int index = ParsingUtilities.ParseIntFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));
                    spacePos = line.IndexOf(" ");
                    string element = line.Substring(0, spacePos);
                    var type = ElementSymbols.GetFromSymbol(new string(
                                                                element.Where(c => char.IsLetter(c))
                                                                       .ToArray()));
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));
                    line = TrimWhiteSpace(line);
                    spacePos = line.IndexOf(" ");
                    float x = ParsingUtilities.ParseFloatFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));

                    line = TrimWhiteSpace(line);
                    spacePos = line.IndexOf(" ");
                    float y = ParsingUtilities.ParseFloatFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));

                    line = TrimWhiteSpace(line);
                    spacePos = line.IndexOf(" ");
                    float z = ParsingUtilities.ParseFloatFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));

                    var pose =
                        new PointTransformation(new Vector3(x, y, z) * 0.1f);
                    data.Particles.Add(new Atom(type.Value, (uint) (index - 1), pose));
                    //Debug.Log("type: " + type + " index: " + index + " position: " + pose.Position);
                    line = reader.ReadLine();
                    line = TrimWhiteSpace(line);
                }
            }

            while (!line.StartsWith("@"))
            {
                line = reader.ReadLine();
            }

            if (line.StartsWith(mol2Bond))
            {
                line = reader.ReadLine();
                if (line == null)
                {
                    reader.Close();
                    return;
                }

                line = TrimWhiteSpace(line);
                while (line.Length != 0)
                {
                    int spacePos = line.IndexOf(" ");
                    int index = ParsingUtilities.ParseIntFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));
                    line = TrimWhiteSpace(line);
                    spacePos = line.IndexOf(" ");
                    int pAIndex = ParsingUtilities.ParseIntFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));
                    line = TrimWhiteSpace(line);
                    spacePos = line.IndexOf(" ");
                    int pBIndex = ParsingUtilities.ParseIntFromSubstring(line, 0, spacePos);
                    line = line.Substring(spacePos + 1, line.Length - (spacePos + 1));
                    line = TrimWhiteSpace(line);
                    spacePos = line.IndexOf(" ");
                    if (spacePos < 0)
                    {
                        spacePos = line.Length;
                    }

                    string bondOrderMaybe =
                        new string(
                            line.Substring(0, spacePos).Where(c => char.IsDigit(c)).ToArray());
                    int bondOrder = -1;
                    if (bondOrderMaybe.Length != 0)
                    {
                        bondOrder =
                            int.Parse(
                                new string(bondOrderMaybe.Where(c => char.IsDigit(c)).ToArray()), CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        if (line.Substring(0, spacePos) == "am")
                        {
                            bondOrder = 1;
                        }
                    }

                    data.Bonds.Add(new Bond((uint) (index - 1),
                                            data.Particles[pAIndex - 1],
                                            data.Particles[pBIndex - 1], bondOrder));
                    line = reader.ReadLine();
                    if (line == null)
                    {
                        FixAromatics(data);
                        reader.Close();
                        return;
                    }

                    line = TrimWhiteSpace(line);
                }
            }
        }

        private static void FixAromatics(BondsAndParticles data)
        {
            SetAromaticsToDoubleOrSingle(data);
            WriteCorrectBondsForAromatics(data);
        }

        private static void SetAromaticsToDoubleOrSingle(BondsAndParticles data)
        {
            List<Bond> aromaticBonds = data.Bonds.Where(b => b.BondOrder == -1).ToList();
            Bond startBond = aromaticBonds.FirstOrDefault(b => b.A is Atom atom &&
                                                               atom.Element == Element.Carbon &&
                                                               b.B is Atom atom2 &&
                                                               atom2.Element == Element.Carbon);
            if (startBond == null)
            {
                startBond = aromaticBonds.FirstOrDefault();
            }

            if (startBond == null)
            {
                return;
            }

            var nextBonds = new List<Bond>();
            int depth = 0;
            while (aromaticBonds.Count > 0 && depth < 10)
            {
                var bondsToRemove = new List<Bond>(new Bond[] { startBond });
                startBond.BondOrder = -2;
                foreach (Bond b in aromaticBonds)
                {
                    if (b.SharesBondedParticle(startBond) && b != startBond)
                    {
                        b.BondOrder = -3;
                        bondsToRemove.Add(b);
                        foreach (Bond bb in aromaticBonds)
                        {
                            if (b.SharesBondedParticle(bb) && bb != startBond && bb != b)
                            {
                                nextBonds.Add(bb);
                            }
                        }
                    }
                }

                nextBonds = nextBonds.Where(b => b.BondOrder == -1).ToList();
                aromaticBonds = aromaticBonds.Except(bondsToRemove).ToList();
                startBond = nextBonds.FirstOrDefault(b => b.A is Atom atom &&
                                                          atom.Element == Element.Carbon &&
                                                          b.B is Atom atom2 &&
                                                          atom2.Element == Element.Carbon);
                if (startBond == null)
                {
                    startBond = nextBonds.FirstOrDefault();
                }

                if (startBond == null)
                {
                }

                if (startBond == null)
                {
                    startBond = aromaticBonds.FirstOrDefault(b => b.A is Atom atom &&
                                                                  atom.Element == Element.Carbon &&
                                                                  b.B is Atom atom2 &&
                                                                  atom2.Element == Element.Carbon);
                    if (startBond == null)
                    {
                        startBond = aromaticBonds.FirstOrDefault();
                    }

                    if (startBond == null)
                    {
                        return;
                    }

                    nextBonds = new List<Bond>();
                    depth = 0;
                    continue;
                }

                nextBonds.Remove(startBond);
                depth++;
                if (depth == 10)
                {
                    Debug.Log("went too deep");
                }
            }
        }

        private static void WriteCorrectBondsForAromatics(BondsAndParticles data)
        {
            foreach (Bond b in data.Bonds)
            {
                if (b.BondOrder < 0)
                {
                    b.BondOrder += 4;
                }
            }
        }

        private static string TrimWhiteSpace(string toTrim)
        {
            int indexOfFirstRealChar = toTrim.TakeWhile(c => char.IsWhiteSpace(c)).Count();
            toTrim = toTrim.Substring(indexOfFirstRealChar, toTrim.Length - (indexOfFirstRealChar));
            return toTrim;
        }

        private static BondsAndParticles ImportPDB(string path)
        {
            var frameData = new BondsAndParticles();
            ReadImportFilePDB(path, frameData);
            return frameData;
        }

        private static void ReadImportFilePDB(string path, BondsAndParticles data)
        {
            //this doesnt do double/triple bond stuff yet
            StreamReader reader = new StreamReader(path);
            string line = reader.ReadLine();

            while (!line.StartsWith(PDBTer) && !line.StartsWith(PDBConnect) && line != null)
            {
                if (line.StartsWith(PDBHetaAtom) || line.StartsWith(PDBAtom))
                {
                    float x = float.Parse(line.Substring(30, 8).Trim(),
                                          CultureInfo.InvariantCulture);
                    float y = float.Parse(line.Substring(38, 8).Trim(),
                                          CultureInfo.InvariantCulture);
                    float z = float.Parse(line.Substring(46, 8).Trim(),
                                          CultureInfo.InvariantCulture);

                    var position = new Vector3(x, y, z);
                    var pose =
                        new PointTransformation(position * NanoScale);
                    //dont need to convert to local space ^^ above, because my output was in local, and particles are already in local
                    var type = ElementSymbols.GetFromSymbol(line.Substring(76, 2).Trim());
                    int index = ParsingUtilities.ParseIntFromSubstring(line, 6, 5) - 1;
                    var newParticle = new Atom(type.Value, (uint) index, pose);
                    data.Particles.Add(newParticle);
                }

                line = reader.ReadLine();
            }

            while (!line.StartsWith(PDBEnd))
            {
                if (line.StartsWith(PDBConnect))
                {
                    int mainIndex = ParsingUtilities.ParseIntFromSubstring(line, 6, 5) - 1;
                    int readerIndex = 11;

                    while (readerIndex < 30)
                    {
                        try
                        {
                            if (line.Substring(readerIndex, 5).Trim() == "")
                                break;
                        }
                        catch
                        {
                            break;
                        }

                        int connectedIndex = ParsingUtilities.ParseIntFromSubstring(line, readerIndex, 5) - 1;
                        Bond newBond = new Bond(0, data.Particles[mainIndex],
                                                data.Particles[connectedIndex]);
                        if (!newBond.IdenticalBondExistsInList(data.Bonds))
                        {
                            data.Bonds.Add(newBond);
                        }

                        readerIndex += 5;
                    }
                }

                line = reader.ReadLine();
            }

            reader.Close();
        }

        #endregion


        public static float AngstromScale = 1f; //from nano to angstrom

        public static float NanoScale = 1f; //from angstrom to nano WHY ARE THESE BROKEN??

        //mol2 constants

        public static string mol2MoleculeType = "@<TRIPOS>MOLECULE";
        public static string mol2Atom = "@<TRIPOS>ATOM";

        public static string mol2Bond = "@<TRIPOS>BOND";


        //PDB constants
        public static string PDBHeaderStart = "HEADER";
        public static string PDBHeaderDescription = "EXPORTED WITH NARUPA BUILDER";
        public static string PDBHeaderId = "ID0";
        public static string PDBHetaAtom = "HETATM";
        public static string PDBAtom = "ATOM";
        public static string PDBConnect = "CONECT";
        public static string PDBTer = "TER";
        public static string PDBEnd = "END";
        public static int PDBLength = 80;
        public static string DefaultPDBGroup = "R";
        public static int DefaultPDBGroupIndex = 1;
    }
}