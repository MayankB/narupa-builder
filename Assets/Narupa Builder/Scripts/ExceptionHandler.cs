using UnityEngine;
using UnityEngine.Assertions;

namespace Narupa_Builder.Scripts
{
    /// <summary>
    /// Display exceptions logged to Unity as notifications in SteamVR.
    /// </summary>
    public class ExceptionHandler : MonoBehaviour
    {
        [SerializeField]
        private NotificationManager notifications;

        private void Awake()
        {
            Assert.IsNotNull(notifications);
            Application.logMessageReceived += HandleException;
        }

        private void HandleException(string condition, string stackTrace, LogType type)
        {
            if (type == LogType.Exception)
            {
                notifications.ShowNegativeNotification(condition);
            }
        }
    }
}