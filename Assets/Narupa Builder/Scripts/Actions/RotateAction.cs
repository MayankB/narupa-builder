﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using Narupa.Core.Math;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NarupaBuilder
{
    public class RotateAction : BaseAction
    {
        private Transformation startPose;
        private Particle mainRotate;
        private Particle rotateAxis = null;
        private List<Particle> particlesToRotate = new List<Particle>();
        private List<Particle> rotatedParticles = new List<Particle>();

        public RotateAction(ActionsManager aM, VisualisationManager vM) : base(aM, vM)
        {
        }

        private Bond hitBond;

        public override void UpdateActionPrepared()
        {
            base.UpdateActionPrepared();
            hitBond = GetHighlightedBondsInOrder().FirstOrDefault();
            if (hitBond != null)
            {
                AssignChainedParticlesFromBond(hitBond, SimulationSpacePose);
                if (particlesToRotate.Count == 0)
                {
                    //TODO: Reimplement
                    Actions.ActionErrorText = "Can't Rotate In Ring";
                }
            }
        }

        public override void OnActionStarted()
        {
            base.OnActionStarted();
            startPose = SimulationSpacePose;
        }

        public override void UpdateActionInProgress()
        {
            base.UpdateActionInProgress();
            if (particlesToRotate.Count != 0)
            {
                var pose = SimulationSpacePose;
                (var startTrans, var endTrans) = GetRotateTransformations(pose);
                rotatedParticles = particlesToRotate.TransformParticles(startTrans, endTrans);
            }
        }

        public override void RenderActionInProgress(BondsAndParticles currentSystem,
                                                    BondsAndParticles currentSelection)
        {
            currentSystem.SetParticlePoseToMatchingParticlesPose(rotatedParticles);
            toRotateVisualiser.SetBondsAndParticles(rotatedParticles, currentSystem.Bonds
                                                                                   .BondsContainingAnyParticlesByIndex(
                                                                                       rotatedParticles));
            axisVisualiser.SetBondsAndParticles(new Particle[0],
                                                new Bond(0, rotateAxis, mainRotate).AsList());
        }

        public override void OnActionFinished()
        {
            base.OnActionFinished();
            if (rotatedParticles.Any())
            {
                CurrentSystem.SetParticlePoseToMatchingParticlesPose(rotatedParticles);
                Actions.SetSystemDirty();
            }

            ClearUp();
        }

        public override void OnActionCancelled()
        {
            base.OnActionCancelled();
            ClearUp();
        }

        private void ClearUp()
        {
            Actions.ActionErrorText = "";
            toRotateVisualiser.Destroy();
            axisVisualiser.Destroy();
        }

        private IBuilderVisualiser toRotateVisualiser;
        private IBuilderVisualiser axisVisualiser;

        public override void OnActionPrepared()
        {
            base.OnActionPrepared();
            toRotateVisualiser = Visualisations.RotateHighlight;
            axisVisualiser = Visualisations.RotateHighlightAxis;
        }

        public override void RenderActionPrepared(BondsAndParticles currentSystem,
                                                  BondsAndParticles currentSelection)
        {
            base.RenderActionPrepared(currentSystem, currentSelection);
            if (hitBond == null)
            {
                toRotateVisualiser.ClearFrame();
                axisVisualiser.ClearFrame();
            }
            else
            {
                toRotateVisualiser.SetBondsAndParticles(particlesToRotate,
                                                        currentSystem
                                                            .Bonds
                                                            .BondsContainingAnyParticlesByIndex(
                                                                particlesToRotate));
                axisVisualiser.SetBondsAndParticles(new Particle[0], hitBond.AsList());
            }
        }

        private (Transformation startTrans, Transformation endTrans) GetRotateTransformations(
            Transformation current)
        {
            float controllerAxisRotationAngle =
                Mathf.DeltaAngle(startPose.Rotation.eulerAngles.z, current.Rotation.eulerAngles.z);

            Vector3 controllerForwardStructureSpace = startPose.Rotation * Vector3.forward;

            Transformation startBondTransformation = GetBondTransformation(mainRotate, rotateAxis);

            Transformation endBondTransformation = startBondTransformation;

            Vector3 bondAxisForward = startBondTransformation.Rotation * Vector3.forward;

            float bondAxisRotationAngle = controllerAxisRotationAngle *
                                          Mathf.Sign(
                                              Vector3.Dot(bondAxisForward,
                                                          controllerForwardStructureSpace));

            bondAxisRotationAngle *=
                ((Mathf.Pow(Mathf.Abs(bondAxisRotationAngle), 0.3f)) / 4f); //this can be modified

            Quaternion bondRotation = Quaternion.AngleAxis(bondAxisRotationAngle, Vector3.forward);
            endBondTransformation.Rotation *= bondRotation;
            return (startBondTransformation, endBondTransformation);
        }

        private Transformation GetBondTransformation(Particle a, Particle b)
        {
            Vector3 bondAxisForward = (b.Pose.Position - a.Pose.Position).normalized;
            Vector3 bondAxisUp = (Vector3.Cross(bondAxisForward, Random.onUnitSphere)).normalized;

            Vector3 position = a.Pose.Position;
            Quaternion rotation = Quaternion.LookRotation(bondAxisForward, bondAxisUp);
            Vector3 scale = Vector3.one;

            return new Transformation(position, rotation, scale);
        }

        private void AssignChainedParticlesFromBond(Bond hitBond, Transformation pose)
        {
            Vector3 controllerDirection = pose.Rotation * Vector3.forward;
            if (Vector3.Dot(hitBond.GetBondDirectionFromAToB(), controllerDirection) < 0)
            {
                mainRotate = hitBond.B;
                rotateAxis = hitBond.A;
            }
            else
            {
                mainRotate = hitBond.A;
                rotateAxis = hitBond.B;
            }

            HashSet<Particle> rotateGroup = new HashSet<Particle>(new Particle[] { rotateAxis });
            List<Particle> lastTest = new List<Particle>(new Particle[] { rotateAxis });
            for (int i = 0; i < 100; i++)
            {
                List<Particle> nextTest = new List<Particle>();
                nextTest = lastTest.SelectMany(p => p.BondedParticles).ToList();
                if (i == 0)
                {
                    nextTest.Remove(mainRotate);
                }

                nextTest = nextTest.Except(rotateGroup).ToList();
                if (nextTest.Contains(mainRotate))
                {
                    Debug.Log("Axis not valid, loops back on its self");
                    particlesToRotate = new List<Particle>();
                    return;
                }

                if (rotateGroup.Intersect(nextTest).Count() != 0)
                {
                    throw new InvalidOperationException(
                        "there should never be a duplicate being added to this list");
                }

                rotateGroup.UnionWith(nextTest);
                lastTest.Clear();
                lastTest.AddRange(nextTest);
            }

            particlesToRotate = rotateGroup.ToList();
        }
    }
}