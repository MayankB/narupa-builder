// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;

namespace NarupaBuilder
{
    /// <summary>
    /// Something which prevents actions until it is closed.
    /// </summary>
    public interface IModal
    {
        bool CanBeReplacedBy(IModal newModal);

        void OnCloseModal();

        void Dismiss();
        
        bool HideMolecule { get; }
    }
}