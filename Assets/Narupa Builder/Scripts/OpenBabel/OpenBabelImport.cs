using OpenBabel;

namespace NarupaBuilder.OpenBabel
{
    /// <summary>
    /// Imports files using OpenBabel.
    /// </summary>
    public static class OpenBabelImport
    {
        /// <summary>
        /// Import a file using OpenBabel.
        /// </summary>
        public static BondsAndParticles Import(string filename)
        {
            var conv = new OBConversion(filename);
            var mol = new OBMol();
            conv.ReadFile(mol, filename);
            return mol.AsBondsAndParticles();
        }
        
    }
}