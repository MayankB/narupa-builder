using System;
using OpenBabel;

namespace NarupaBuilder.OpenBabel
{
    /// <summary>
    /// Utility methods for setting up OpenBabel operations.
    /// </summary>
    public static class OpenBabelSetup
    {
        public static Version RequiredVersion = new Version("3.0.0");
        /// <summary>
        /// Indicates whether OpenBabel is available on the system.
        /// </summary>
        public static bool IsOpenBabelAvailable
        {
            get
            {
                if(!checkedForOpenBabel)
                    CheckForOpenBabel();
                return isOpenBabelAvailable;
            }
        }

        private static bool checkedForOpenBabel;
        private static bool isOpenBabelAvailable;
        private static string openBabelVersion = null;
        
        public static void ThrowOpenBabelNotFoundError()
        {
            if(openBabelVersion != null)
                throw new OpenBabelException($"OpenBabel v{RequiredVersion}+ is required, " +
                                             $"v{openBabelVersion} was found.");
            throw new OpenBabelException($"OpenBabel v{RequiredVersion}+ not found, is it on your PATH?");
        }
        
        private static void CheckForOpenBabel()
        {
            try
            {
                openBabelVersion = openbabel_csharp.OBReleaseVersion();
                isOpenBabelAvailable = new Version(openBabelVersion) >= RequiredVersion;
            } 
            catch (TypeInitializationException)
            {
                isOpenBabelAvailable = false;
            }

            checkedForOpenBabel = true;
        }
    }
}