﻿// Copyright (c) Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Narupa.Core.Math;
using Narupa.Core.Science;
using NUnit.Framework;
using UnityEngine;
using MatrixPair = System.Tuple<UnityEngine.Matrix4x4, UnityEngine.Matrix4x4>;

namespace NarupaBuilder.Tests
{
    ///<summary>
    /// This Class deals with the tests for the MathMain class
    ///</summary>
    public class HitChecksTests
    {
        public class ParticlesHitByPositionBiasedTestData
        {
            public ParticlesHitByPositionBiasedTestData(float bufferRangeForHit,
                                                        Vector3 hitPoint,
                                                        List<Particle> ParticlesToCheck,
                                                        List<Particle>
                                                            expectedClosestOrderOfParticle)
            {
                this.bufferRangeForHit = bufferRangeForHit;
                this.hitPoint = hitPoint;
                this.ParticlesToCheck = ParticlesToCheck;
                this.expectedClosestOrderOfParticle = expectedClosestOrderOfParticle;
            }

            public float bufferRangeForHit;
            public Vector3 hitPoint;
            public List<Particle> ParticlesToCheck;
            public List<Particle> expectedClosestOrderOfParticle;
        }

        public class BondsHitByPositionUnBiasedTestData
        {
            public BondsHitByPositionUnBiasedTestData(float bufferRangeForHit,
                                                      Vector3 hitPoint,
                                                      List<Bond> bondsToCheck,
                                                      List<Bond> expectedClosestOrderOfBonds)
            {
                this.bufferRangeForHit = bufferRangeForHit;
                this.hitPoint = hitPoint;
                this.bondsToCheck = bondsToCheck;
                this.expectedClosestOrderOfBonds = expectedClosestOrderOfBonds;
            }

            public float bufferRangeForHit;
            public Vector3 hitPoint;
            public List<Bond> bondsToCheck;
            public List<Bond> expectedClosestOrderOfBonds;
        }

        static List<Particle> whichParticlesToCheck = new List<Particle>(new Particle[]
        {
            new Atom(Element.Carbon, 0,
                     new PointTransformation(new Vector3(0, 0, 0))),
            new Atom(Element.Carbon, 1,
                     new PointTransformation(new Vector3(.25f, 0, 0))),
            new Atom(Element.Carbon, 2,
                     new PointTransformation(new Vector3(5, 0, 0))),
            new Atom(Element.Carbon, 3,
                     new PointTransformation(new Vector3(-5, 0, 0))),
            new Atom(Element.Carbon, 4,
                     new PointTransformation(new Vector3(.75f, 0, 0))), //should add ones with bias later
        });

        public static ParticlesHitByPositionBiasedTestData[] ParticlesHitByPositionBiasedTest =
            new[]
            {
                new ParticlesHitByPositionBiasedTestData(.1f, new Vector3(.25f, 0, 0),
                                                         whichParticlesToCheck,
                                                         new List<Particle>(new Particle[]
                                                         {
                                                             new Atom(Element.Carbon, 1,
                                                                      new PointTransformation(
                                                                          new Vector3(.25f, 0, 0))),
                                                             new Atom(Element.Carbon, 0,
                                                                      new PointTransformation(
                                                                          new Vector3(0, 0, 0))),
                                                             new Atom(Element.Carbon, 4,
                                                                      new PointTransformation(
                                                                          new Vector3(.75f, 0, 0)))
                                                         })),
                new ParticlesHitByPositionBiasedTestData(100f, new Vector3(0, 0, -10),
                                                         whichParticlesToCheck,
                                                         new List<Particle>(new Particle[]
                                                         {
                                                             new Atom(Element.Carbon, 0,
                                                                      new PointTransformation(
                                                                          new Vector3(0, 0, 0))),
                                                             new Atom(Element.Carbon, 1,
                                                                      new PointTransformation(
                                                                          new Vector3(.25f, 0, 0))),
                                                             new Atom(Element.Carbon, 4,
                                                                      new PointTransformation(
                                                                          new Vector3(.75f, 0, 0))),
                                                             new Atom(Element.Carbon, 2,
                                                                      new PointTransformation(
                                                                          new Vector3(5, 0, 0))),
                                                             new Atom(Element.Carbon, 3,
                                                                      new PointTransformation(
                                                                          new Vector3(-5, 0, 0)))
                                                         })),
                new ParticlesHitByPositionBiasedTestData(0.1f, new Vector3(0, 0, -10),
                                                         whichParticlesToCheck,
                                                         new List<Particle>(new Particle[]
                                                         {
                                                         }))
            };

        static List<Particle> ParticlesForBondTests = new List<Particle>(new Particle[]
        {
            new Atom(Element.Hydrogen, 0,
                     new PointTransformation(new Vector3(.5f, 0, 0))),
            new Atom(Element.Hydrogen, 1,
                     new PointTransformation(new Vector3(-.5f, 0, 0))),
            new Atom(Element.Hydrogen, 2,
                     new PointTransformation(new Vector3(0, .5f, .1f))),
            new Atom(Element.Hydrogen, 3,
                     new PointTransformation(new Vector3(0, -.5f, .1f))),
            new Atom(Element.Hydrogen, 4,
                     new PointTransformation(new Vector3(.5f, 0, .5f))),
            new Atom(Element.Hydrogen, 5,
                     new PointTransformation(new Vector3(.5f, 0, -.5f))),
        });

        static List<Bond> whichBondsToCheck = new List<Bond>(new Bond[]
        {
            new Bond(0, ParticlesForBondTests[0], ParticlesForBondTests[1]),
            new Bond(1, ParticlesForBondTests[2], ParticlesForBondTests[3]),
            new Bond(2, ParticlesForBondTests[4], ParticlesForBondTests[5]),
        });

        public static BondsHitByPositionUnBiasedTestData[] bondsHitByPositionUnbiasedTest = new[]
        {
            new BondsHitByPositionUnBiasedTestData(.1f, new Vector3(0, 0, 0), whichBondsToCheck,
                                                   new List<Bond>(new Bond[]
                                                   {
                                                       whichBondsToCheck[0],
                                                       whichBondsToCheck[1],
                                                   })),
            new BondsHitByPositionUnBiasedTestData(100f, new Vector3(100f, 0, 0), whichBondsToCheck,
                                                   new List<Bond>(new Bond[]
                                                   {
                                                       whichBondsToCheck[2],
                                                       whichBondsToCheck[1],
                                                   })),
            new BondsHitByPositionUnBiasedTestData(.1f, new Vector3(.51f, 0, 0), whichBondsToCheck,
                                                   new List<Bond>(new Bond[]
                                                   {
                                                       whichBondsToCheck[2],
                                                   })),
        };

        [Test]
        public void TestingBondsHitPoistionUnbiased(
            [ValueSource(nameof(bondsHitByPositionUnbiasedTest))]
            BondsHitByPositionUnBiasedTestData bondsHitByPositionUnbiasedTest)
        {
            Sphere hitDataForTest = new Sphere(bondsHitByPositionUnbiasedTest.hitPoint,
                                               bondsHitByPositionUnbiasedTest.bufferRangeForHit);
            List<Bond> listOfHitBonds =
                HitChecks.GetSortedBondsHit(bondsHitByPositionUnbiasedTest.bondsToCheck,
                                            hitDataForTest);
            for (int i = 0;
                 i < bondsHitByPositionUnbiasedTest.expectedClosestOrderOfBonds.Count;
                 i++)
            {
                Assert.AreEqual(
                    bondsHitByPositionUnbiasedTest.expectedClosestOrderOfBonds[i].CurrentIndex,
                    listOfHitBonds[i].CurrentIndex);
            }
        }
    }
}