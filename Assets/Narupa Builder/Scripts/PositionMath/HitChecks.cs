﻿// Copyright (c) 2019 Intangible Realities Lab. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// Utility methods used for proximity, hit detection and selection using
    /// particles.
    /// </summary>
    public static class HitChecks
    {
        /// <summary>
        /// Get the particles which overlap with the given sphere, sorted by their distance
        /// to the sphere's origin.
        /// </summary>
        public static IOrderedEnumerable<TParticle> SortedParticlesWhichOverlapSphere<TParticle>(
            Sphere sphere,
            IEnumerable<TParticle> particles)
            where TParticle : Particle
        {
            return particles.WhichOverlapSphere(sphere)
                                   .OrderBy(particle => Vector3.SqrMagnitude(particle.Position -
                                                                             sphere.Origin));
        }

        public static List<Particle> BiasedSortedParticlesBondOverlap(Particle hitParticle,
                                                                      List<Particle>
                                                                          particlesToCheck)
        {
            return ReduceListOnParticleType(hitParticle, particlesToCheck
                                                         .Where(p => DoBondsOverlap(hitParticle, p,
                                                                                    1.5f))
                                                         .OrderBy(p => GetBondNumberBiasedToPoint(
                                                                                                  p,
                                                                                                  hitParticle
                                                                                                      .Pose
                                                                                                      .Position))
                                                         .ToList());
        }

        public static IOrderedEnumerable<Particle> SortedParticlesToPoint(Vector3 point,
                                                            IEnumerable<Particle> particles)
        {
            return particles
                   .OrderBy(particle => Vector3.SqrMagnitude(point - particle.Position));
        }


        public static List<Bond> GetSortedBondsHit(List<Bond> bondsToCheck,
                                                   Sphere hitCheckData,
                                                   float bondRadius = 0.0125f)
        {
            return bondsToCheck
                   .Where(bond => Geometry.CheckIfPointCanFormRightAngleWithLineSegment(
                                                                                        hitCheckData
                                                                                            .Origin,
                                                                                        ProduceModifiedLineSegment(bond)))
                   .Select(
                           bond => (bond,
                                    distance: Geometry.GetClosestDistanceToLineSegmentCylinder(
                                                                                               hitCheckData
                                                                                                   .Origin,
                                                                                               ProduceModifiedLineSegment(bond))))
                   .Where(pair => pair.distance < bondRadius + hitCheckData.Radius)
                   .OrderBy(pair => pair.distance)
                   .Select(pair => pair.bond)
                   .ToList();
        }

        private static List<Particle> ReduceListOnParticleType(Particle particle,
                                                               List<Particle> toReduce)
        {
            var primary = toReduce
                          .Take(particle.CommonBondNumber)
                          .ToList();
            primary.AddRange(toReduce.Where(p => DoBondsOverlap(particle, p, 1.0f)).ToList());
            return primary.Distinct().ToList();
        }

        /// <summary>
        /// Get any particles which overlap the given sphere.
        /// </summary>
        public static IEnumerable<TParticle> WhichOverlapSphere<TParticle>(
            this IEnumerable<TParticle> particles,
            Sphere sphere)
            where TParticle : Particle
        {
            return particles.Where(particle => DoesSphereOverlapParticle(particle, sphere));
        }

        private static bool DoBondsOverlap(Particle hitParticle, Particle checkParticle, float bias)
        {
            if (bias > 1f && checkParticle.BondedParticles.Count >=
                checkParticle.CommonBondNumber)
                bias = 1f;

            var bondSummation =
                ElementValues.GetIdealBondLength(hitParticle as Atom,
                                                                            checkParticle as Atom,
                                                                            1);
            return Vector3.Distance(hitParticle.Pose.Position, checkParticle.Pose.Position) <
                   bondSummation * bias;
        }

        private static float GetBondNumberBiasedToPoint(Particle particle, Vector3 point)
        {
            var bias = 1f;
            if (particle.BondedParticles.Count >= particle.CommonBondNumber) bias = 0.5f;

            return Vector3.Distance(particle.Pose.Position, point) / bias;
        }

        /// <summary>
        /// Check if a particle overlaps a sphere.
        /// </summary>
        private static bool DoesSphereOverlapParticle(Particle particle, Sphere hitSphere)
        {
            return Sphere.DoOverlap(hitSphere, particle.AsSphere());
        }

        /// <summary>
        /// Convert a <see cref="Particle" /> to a sphere located at the position of the
        /// particle, with a diameter equal to the particle's scale.
        /// </summary>
        private static Sphere AsSphere(this Particle particle)
        {
            return new Sphere(particle.Pose.Position, particle.Scale / 2f);
        }

        private static LineSegment ProduceModifiedLineSegment(Bond checkingThisBond)
        {
            var bondEndPositionA = checkingThisBond.A.Pose.Position;
            var bondEndPositionB = checkingThisBond.B.Pose.Position;
            var bondSlopeFromAtoB = Vector3.Normalize(bondEndPositionB - bondEndPositionA);
            var particleAScale = checkingThisBond.A.Scale / 8f;
            var particleBScale = checkingThisBond.B.Scale / 8f;

            var pointWhereBondIntersectsSphereA =
                bondEndPositionA + bondSlopeFromAtoB * particleAScale;
            var pointWhereBondIntersectsSphereB =
                bondEndPositionB + bondSlopeFromAtoB * -1f * particleBScale;
            var returnSegment =
                new LineSegment(pointWhereBondIntersectsSphereA, pointWhereBondIntersectsSphereB);
            return returnSegment;
        }
    }
}