using Narupa.Core.Math;
using UnityEngine;

namespace NarupaBuilder
{
    /// <summary>
    /// A transformation that has unit scale and no rotation.
    /// </summary>
    public struct PointTransformation
    {
        public Vector3 Position;
        public float Scale;
        public static PointTransformation Identity => new PointTransformation(Vector3.zero);

        public PointTransformation(Vector3 position, float scale = 1)
        {
            Position = position;
            Scale = scale;
        }
        
        public Matrix4x4 Matrix =>
            Matrix4x4.TRS(Position, Quaternion.identity, Scale * Vector3.one);

        public static PointTransformation operator *(PointTransformation a, PointTransformation b)
        {
            return new PointTransformation()
            {
                Position = a.Position + a.Scale * b.Position,
                Scale = a.Scale * b.Scale
            };
        }
        
        public static PointTransformation operator +(PointTransformation a, Vector3 b)
        {
            return new PointTransformation()
            {
                Position = a.Position + b,
                Scale = a.Scale
            };
        }
        
        public static PointTransformation operator *(float a, PointTransformation b)
        {
            return new PointTransformation()
            {
                Position = a * b.Position,
                Scale = a * b.Scale
            };
        }
        
        public static PointTransformation operator *(Quaternion a, PointTransformation b)
        {
            return new PointTransformation()
            {
                Position = a * b.Position,
                Scale = b.Scale
            };
        }
        
        /// <summary>
        /// This is not always true - should be replaced!
        /// </summary>
        public static PointTransformation operator *(Matrix4x4 a, PointTransformation b)
        {
            return new PointTransformation()
            {
                Position = a.MultiplyPoint3x4(b.Position),
                Scale = a.lossyScale.x * b.Scale
            };
        }
    }
}